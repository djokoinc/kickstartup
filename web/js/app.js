function scrollto(element){
    $(element).click(function(){  
        var goscroll = false;  
        var the_hash = $(this).attr("href");  
        var regex = new RegExp("\#(.*)","gi");  
        var the_element = '';  
   
        if(the_hash.match("\#(.+)")) {
            the_hash = the_hash.replace(regex,"$1");  
   
            if($("#"+the_hash).length>0) {  
                the_element = "#" + the_hash;  
                goscroll = true;  
            }else if($("a[name=" + the_hash + "]").length>0) {  
                the_element = "a[name=" + the_hash + "]";  
                goscroll = true;  
            }
            if(goscroll) {  
                $('html, body').animate({  
                    scrollTop:$(the_element).offset().top  
                }, 500);  
                return false;  
            }
        } 
    });  
};  
scrollto('a[href^="#"]');

$('div.burger').click(function(){
    $('nav.menu').slideToggle();
    $('div.burger div.stripe').toggleClass('click');
});

function toggle(action, target){
    $(action).click(function(){
        $(target).toggle();
    });
}

//$('#modal').click(function(){
//    $(this).hide(function(){
//        $('.littleModal').hide();
//    });
//});

$('#login').click(function(){
    $('div.loginForm').toggle();
    $("#modal").toggle();
});

$('div.howdoesitwork').click(function(){
    $("#ccm").toggle();
    $("#modal").toggle();
    $('html,body').animate({scrollTop: $("#ccm")},'slow');
});

$('.close').click(function(){
    $(this).parent().parent().hide(); 
    $("#modal").hide();
});

$('input').focus(function(){
     $(this).animate({opacity: 0.5}, 300);
});
//
//$('input').blur(function(){
//    if($(this).val() === ''){
//        $('input').animate({opacity: 0.5}, 300);
//    }else{
//        $('input').animate({opacity: 1}, 300);
//    }
//});