function initialize(id) {
  var element = document.getElementById(id);
  if (element) {
    var autocomplete = new google.maps.places.Autocomplete(element, { types: ['geocode'] });
    google.maps.event.addListener(autocomplete, 'place_changed', onPlaceChanged);
  }
}

function onPlaceChanged() {
  var place = this.getPlace();
  // console.log(place);  // Uncomment this line to view the full object returned by Google API.

  for (var i in place.address_components) {
    var component = place.address_components[i];
    for (var j in component.types) {  // Some types are ["country", "political"]
      var type_element = document.getElementById(component.types[j]);
      if (type_element) {
        type_element.value = component.long_name;
      }
//        if (document.getElementById("street_number")) {
//          document.getElementById("street_number").value = place.address_components["street_number"].long_name;
//        }
    }
  }

//    { "long_name": "25",            "types": [ "street_number" ] },
//    { "long_name": "Petit Musc St", "types": [ "route" ] },
//    { "long_name": "Paris",         "types": [ "locality", "political" ] }
//    if (document.getElementById('street_number')) {
//        this.value = 'long_name';
//      }
}

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = new google.maps.LatLng(
          position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
google.maps.event.addDomListener(window, 'load', function() {
  initialize('user_input_autocomplete_address');
});