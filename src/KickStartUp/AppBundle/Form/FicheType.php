<?php

namespace KickStartUp\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FicheType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title', 'text', array(
                    'label' => 'Nom',
                    'data' => 'DevCorp'
                ))
                ->add('description', 'text', array(
                    'label' => 'Description',
                    'data' => 'Cette start-up ne fonctionnera jamais !'
                ))
                ->add('phone', 'text', array(
                    'label' => 'Téléphone',
                    'data' => '0102030405'
                ))
                ->add('email', 'text', array(
                    'label' => 'Adresse email',
                    'data' => 'devcorp@example.com'
                ))
                ->add('facebook', 'text', array(
                    'label' => 'Facebook',
                    'data' => 'https://www.facebook.com'
                ))
                ->add('twitter', 'text', array(
                    'label' => 'Twitter',
                    'data' => 'https://www.twitter.com'
                ))
                ->add('viadeo', 'text', array(
                    'label' => 'Viadeo',
                    'data' => 'https://www.viadeo.com'
                ))
                ->add('linkedin', 'text', array(
                    'label' => 'LinkedIn',
                    'data' => 'https://www.linkedin.com'
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'KickStartUp\AppBundle\Entity\Fiche'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'kickstartup_appbundle_fiche';
    }

}
