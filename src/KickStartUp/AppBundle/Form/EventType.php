<?php

namespace KickStartUp\AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EventType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title', 'text', array(
                    'label' => 'Titre',
                    'data' => 'Levée de fond de Google'
                ))
                ->add('description', 'text', array(
                    'label' => 'Description',
                    'data' => 'Google a levé des fonds pour sa société nouvellement créée, venez participer à la soirée dédiée à celle-ci.'
                ))
                ->add('city', 'text', array(
                    'label' => 'Ville',
                    'data' => 'Mountain View, CA, United States'
                ))
                ->add('date', 'string', array(
                    'label' => 'Date et heure',
                    'data' => '14 Août 2015, 14h30'
                ))
                ->add('fiche', 'entity', array(
                    'label' => 'Start-Up concernée',
                    'class' => 'KickStartUpAppBundle:Fiche',
                    'property' => 'title',
                ))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'KickStartUp\AppBundle\Entity\Event'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'kickstartup_appbundle_event';
    }

}
