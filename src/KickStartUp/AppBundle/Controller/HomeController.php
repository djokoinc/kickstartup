<?php

namespace KickStartUp\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomeController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $repoF = $em->getRepository('KickStartUpAppBundle:Fiche');
        $repoE = $em->getRepository('KickStartUpAppBundle:Event');
        $kickstartups = $repoF->findAll();
        $events = $repoE->findAll();
        return $this->render('KickStartUpAppBundle:Home:index.html.twig', array(
                'kickstartups' => $kickstartups,
                'events' => $events
            ));    }

}
