<?php

namespace KickStartUp\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class KickStartUpUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
