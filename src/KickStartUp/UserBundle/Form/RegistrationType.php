<?php


namespace KickStartUp\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class RegistrationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('type', 'choice', array(
            'choices'   => array(
                    'expert'   => 'Expert',
                    'investisseur' => 'Investisseur',
                    'startup' => 'Start Up'
                )
            )
        );
    }

    public function getParent()
    {
        return 'fos_user_registration';
    }

    public function getName()
    {
        return 'kick_start_up_user_registration';
    }
}